// src/routes.js
import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom';

import Navigation from './components/Navigation';
import Homepage from './components/Homepage';
import About from './components/About';
import NotFound from './components/NotFound';

const Routes = (props) => (
  <BrowserRouter {...props}>
    <div>
      <Route path="*" component={Navigation} />
      <Switch>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/about" component={About} />
        <Route component={NotFound} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default Routes;
