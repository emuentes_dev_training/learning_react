import React from 'react';
import PropTypes from 'prop-types';

const Welcome = ({name}) => (
  <h2>Welcome to React Workshop{name ? `, ${name}` : ''}</h2>
);

Welcome.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Welcome;
