import React, { Component } from 'react';

class Navigation extends Component {
  render() {
    const pathname = this.props.location.pathname;

    return (
      <div>
        <ul className="nav-links">
          {this.props.pages.map((page) => {
            return (
              <li><a
                href={page.link}
                className={page.link === pathname ? 'active' : ''}
              >{page.label}</a></li>
            );
          })}
        </ul>
      </div>
    )
  }
}

Navigation.defaultProps = {
  pages: [
    {
      label: 'Homepage',
      link: '/'
    },
    {
      label: 'About',
      link: '/about'
    },
  ]
};

export default Navigation;
