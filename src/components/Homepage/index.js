// src/components/Homepage/index.js
import React, { Component } from 'react';
import Welcome from '../Welcome';
import logo from './logo.svg';
import './style.scss';

class Homepage extends Component {
  render() {
    return (
      <div className="homepage">
        <div>
          <div className="homepage-header">
            <img src={logo} className="homepage-logo" alt="logo" />
            <Welcome name="Einstein"/>
          </div>
          <p className="homepage-intro">
            By Edgar Muentes
          </p>
        </div>
      </div>
    );
  }
}

export default Homepage;
